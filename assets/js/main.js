$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;
	
	$('header ul li a[title="Contact"]').attr('data-toggle','modal').attr('href','#contactModal'); 
	
	$('.mobile-drawers ul li a[title="Contact"]').attr('data-toggle','modal').attr('href','#contactModal');  
	
	$(function () {
    var isIE = window.ActiveXObject || "ActiveXObject" in window;
    if (isIE) {
        $('.modal').removeClass('fade');
    }
	});
	
	if($('body.type-1').length){
    $('#home_slider .slider').cycle({
      speed: 2000,
      timeout: 5500,
      fx: 'fade',
      width: '100%',
      fit: 1,
      pager: '.pager',
      pagerTemplate: "<a href=# class=text-hide> {{slideNum}} </a>",
      slides: '> div'
    }); 
	    if($('.pager a').length === 1){
		    $('.pager a').hide();
	    } 
	}
	
	if($('body.type-4').length || $('body.type-13').length){
    var $children = $('#service-modules td');
    for(var i = 0, l = $children.length; i < l; i += 3) {
      $children.slice(i, i+3).wrapAll('<tr></tr>'); 
    }
	}//if 
	
	if($('body.type-5').length){
    $('.facilities #slideshow').cycle({
      speed: 2000,
      timeout: 5500,
      fx: 'fade',
      width: '100%',
      fit: 1,
      prev: '.arrows .prev',
      next: '.arrows .next',
      pager: '.pager',
      pagerTemplate: "<a href='#'><img src='{{src}}'></a>"
    });
    
     var itemCount = $('.pager a').length;
	    if(itemCount <= 1){
	      $('.pager a').hide();
	    }else if(itemCount > 5){
	      var itemWidth = 100/itemCount;
	      $('.pager a').css("width", itemWidth+"%");
	    }
	    
	    
 
	} 
	
	if($('body.type-7').length){
    $('.blogPostFooter').each(function(){
	    $(this).find('.comma:last').hide();
    });
	}//if 
	
	if($('body.type-7').length || $('body.type-10').length){
	   $('#sidebarNav form').hover(function(){
			var originalVal = $(this).find('input[type="text"]').val();
			if(originalVal === "Search"){
				$('#sidebarNav input[type="text"]').val('');
			}
		},function(){
			var newVal = $('#sidebarNav input[type="text"]').val();
			if(newVal == ""){
				$('#sidebarNav input[type="text"]').val("Search");
			}
		});
		var url = window.location.pathname; 
		var yearValue = url.substring(url.lastIndexOf('/') + 1);
		var yearValueHeader = yearValue.replace(/\+/g, ' ');
		if(yearValue != "categories"){
			$('.blogRoll').prepend('<h2>'+yearValueHeader+'</h2>');
		}
		$('#sidebarNav .archives a').each(function(){
			var year = $(this).attr("title");
			if(yearValue == year){
				$(this).parent().addClass('active');
			}
		});
	}//if 
	
	if($('body.page-8').length){
		function getUrlParameter(sParam)
			{
			    var sPageURL = window.location.search.substring(1);
			    var sURLVariables = sPageURL.split('&');
			    for (var i = 0; i < sURLVariables.length; i++) 
			    {
			        var sParameterName = sURLVariables[i].split('=');
			        if (sParameterName[0] == sParam) 
			        {
			            return sParameterName[1];
			        }
			    }
			} 
			var category = getUrlParameter('tag');  
			var categoryHeader = category.replace(/\+/g, ' ');
			$('.blogRoll').prepend('<h2>'+categoryHeader+'</h2>');
			$('#sidebarNav .categories a').each(function(){
				var title = $(this).attr("title");
				if(category == title){
					$(this).parent().addClass('active'); 
				}
			});
		
	}//if
	

	
	
	if($('body.type-3').length){
    $('.bio').readmore({
	    lessLink: '<a class="generalButton yellow" href="">View Less</a>', 
	    moreLink: '<a class="generalButton yellow" href="">View More</a>',
	    collapsedHeight: 120,
	    afterToggle: function(trigger, element, expanded) {
		    if(! expanded) { // The "Close" link was clicked
		      $('html, body').animate({
					    'scrollTop':   $('#bios').offset().top
					}, 200);
					$(element).find('.readMore').show();  
		    }else{
			    $(element).find('.readMore').hide();  
		    }
		  }
    });
	}//if 
	
	




 
	
  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.mobile-trigger-';
    var content = '#content_container';
    var drawer = '.mobile-drawer-';
    //var contentBorder = '#content_push';
    $('.mobile-drawers').show();
    $(drawer+'left').hide();
    $(drawer+'right').hide();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
	    $(drawer+'left').show(); 
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        }); 

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
	    $(drawer+'right').show();
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//mobileDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.mobile-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.mobile-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }
	/*
$('a[title="Contact"]').click(function(){
		$('#contactModal').modal();
		return false;
	});
*/
	$('a[title="Join"').click(function(){
		//$('#toolkitModal').modal();
		$('input.checked').prop("checked", true);
		//return false;
	});
	/*
$('footer a[title="Personal Training"').click(function(){
		$('#ptModal').modal();
		return false;
	});
	$('footer a[title="Wellness Coaching"').click(function(){
		$('#wcModal').modal();
		return false;
	});
*/
	/*
$('.closeModal').click(function(){
		$('#contactModal').modal('hide');
		$('#toolkitModal').modal('hide');
		$('#ptModal').modal('hide');
		$('#wcModal').modal('hide');
		return false;
	});
*/
}); //End Document Ready